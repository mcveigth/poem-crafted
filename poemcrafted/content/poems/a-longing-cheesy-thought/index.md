---
title: "A Longing(Cheesy) Thought"
date: 2024-05-18T01:20:51-06:00
draft: false
---

_I think about the beauty of the people,_  
_the expressions on their faces,_  
_their smiles,_  
_I think of the silly jokes one makes,_  
_in the short talks that I wish were deep,_  
_I think about loving everyone and not hurting anyone,_  
_I think of the reciprocated and the unreciprocated_  
_I think of the pines darkened by the early night,_  
_I think of her gaze lost in vague thoughts_  
_while mine is lost in her mouth._  
_I think of what's in your life, what makes you happy_  
_I think of when you cry_  
_but most of all I think about how much you're silent._  

The person I made this for told me it was cheesy. Indeed, it is.