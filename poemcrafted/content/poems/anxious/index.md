---
title: "Anxious"
date: 2024-05-18T00:51:30-06:00
draft: false
---

_I need something to ease my sorrows,_   
_soothe my anguish this foreign night._  
_Anxiety eats and roars like a pig,_   
_I need something to make me feel empty._   

_To stop my pulse,_  
_to bring me to calm,_  
_in an endless dream,_  
_in a soulless pause._  

{{< figure src="anxious.png" title="Drawing by Naomi S" height="500" >}}