---
title: "Tal Vez Esos (Maybe Those)"
date: 2025-01-25T13:45:07-07:00
draft: false
---

<h1 style="text-align: center;">
  <span style="color: #FFD700;">Tal Vez Esos</span>
  <span style="color: #C0C0C0;">(Maybe Those)</span>
</h1>

<span style="color: #FFD700;">Y ni un poema,</span>  
<span style="color: #C0C0C0;">And not a poem,</span>  
<span style="color: #FFD700;">ni una palabra,</span>  
<span style="color: #C0C0C0;">not a word,</span>  
<span style="color: #FFD700;">ni un pensamiento</span>  
<span style="color: #C0C0C0;">not a thought</span>  
<span style="color: #FFD700;">que escape de tus labios</span>  
<span style="color: #C0C0C0;">that escapes your lips</span>  
<span style="color: #FFD700;">será sobre mí.</span>  
<span style="color: #C0C0C0;">will be about me.</span>  

<span style="color: #FFD700;">Los sollozos que arroje al viento,</span>  
<span style="color: #C0C0C0;">The sobs I cast to the wind,</span>  
<span style="color: #FFD700;">los gritos que hieran mi pecho,</span>  
<span style="color: #C0C0C0;">the screams that wound my chest,</span>  
<span style="color: #FFD700;">las caricias ausentes, perdidas,</span>  
<span style="color: #C0C0C0;">the absent, lost caresses,</span>  
<span style="color: #FFD700;">esas que huyeron de tus brazos,</span>  
<span style="color: #C0C0C0;">those that fled from your arms,</span>  
<span style="color: #FFD700;">tampoco serán sobre mí.</span>  
<span style="color: #C0C0C0;">will not be about me either.</span>  

<span style="color: #FFD700;">Pensarás en otros recuerdos,</span>  
<span style="color: #C0C0C0;">You will think of other memories,</span>  
<span style="color: #FFD700;">en otra piel,</span>  
<span style="color: #C0C0C0;">of another skin,</span>  
<span style="color: #FFD700;">en otros besos.</span>  
<span style="color: #C0C0C0;">of other kisses.</span>  
<span style="color: #FFD700;">En labios que beben del lago,</span>  
<span style="color: #C0C0C0;">Of lips drinking from the lake,</span>  
<span style="color: #FFD700;">mojados, profundos,</span>  
<span style="color: #C0C0C0;">drenched, profound,</span>  
<span style="color: #FFD700;">ahogados sin fin.</span>  
<span style="color: #C0C0C0;">drowned endlessly.</span>  

<span style="color: #FFD700;">Nadarás como pez en el fuego,</span>  
<span style="color: #C0C0C0;">You will swim like a fish in fire,</span>  
<span style="color: #FFD700;">te hablarás en tu propio infierno,</span>  
<span style="color: #C0C0C0;">speak to yourself in your own hell,</span>  
<span style="color: #FFD700;">y los cuentos que murmures</span>  
<span style="color: #C0C0C0;">and the stories you whisper</span>  
<span style="color: #FFD700;">a oídos lejanos</span>  
<span style="color: #C0C0C0;">to distant ears</span>  
<span style="color: #FFD700;">no serán sobre mí.</span>  
<span style="color: #C0C0C0;">will not be about me.</span>  

<span style="color: #FFD700;">Pero...</span>  
<span style="color: #C0C0C0;">But...</span>  

<span style="color: #FFD700;">Las voces ahogadas,</span>  
<span style="color: #C0C0C0;">The strangled voices,</span>  
<span style="color: #FFD700;">las que paran tu lengua,</span>  
<span style="color: #C0C0C0;">the ones that stop your tongue,</span>  
<span style="color: #FFD700;">las que tiemblan,</span>  
<span style="color: #C0C0C0;">the ones that tremble,</span>  
<span style="color: #FFD700;">las que detienen tus palabras</span>  
<span style="color: #C0C0C0;">the ones that halt your words</span>  
<span style="color: #FFD700;">y te hacen tartamudear...</span>  
<span style="color: #C0C0C0;">and make you stutter...</span>  
<span style="color: #FFD700;">Tal vez esas voces,</span>  
<span style="color: #C0C0C0;">Maybe those voices,</span>  
<span style="color: #FFD700;">tal vez esos recuerdos,</span>  
<span style="color: #C0C0C0;">maybe those memories,</span>  
<span style="color: #FFD700;">tal vez esos poemas,</span>  
<span style="color: #C0C0C0;">maybe those poems,</span>  
<span style="color: #FFD700;">tal vez esos,</span>  
<span style="color: #C0C0C0;">maybe those,</span>  
<span style="color: #FFD700;">sí sean sobre mí.</span>  
<span style="color: #C0C0C0;">will be about me.</span>  