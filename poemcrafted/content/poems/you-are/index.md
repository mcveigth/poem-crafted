---
title: "You Are"
date: 2024-05-18T00:43:19-06:00
draft: false
---

_Mute words, cut through the air_  
_They pierce my palms,_  
_From between my fingers they escape,_  
_I wanted to say something,_  
_But I can't say much of anything,_  
_I didn't expect it to come to this_  
_And you are what I long for,_  
_How I long to glue my words_  
_So my message would get through_  
_Thin as a fine silk rope_  
_Slipping between your ears_  
_As my tears brush my blushing cheeks_  
_And I see you, my face lit up with the depths of your eyes_  
_Soft simple exalts of something indescribable_  
_Love_  
_I love you,_  
_that's all I can say,_  
_And it still scares me to think how much;_  
_Surely I say enough to just look at you_  
_to glue my words_  
_and say_   
_"You're what I long for,_  
_what I want so much,_  
_my smile on sad days."_  