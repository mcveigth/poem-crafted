---
title: "Polyamide"
date: 2024-12-22T16:25:21+00:00
draft: false
---

Lost in a world of perfidious loves     
Treacherous glances, fragrant with decay    
Loveless, upon a bed of lethargy    
Amid columns of fractured crystal   
Enclosed behind alabaster masks     
Among mechanical hummingbirds   
Drowned in lifeless porcelain eyes  
Within crimson tempests 
And piercing bursts of light    
Putrid emotions of crafted agony    
Plastic forms of nylon, ebon threads entwined   
Soaked in oil and the fumes of hollow passion.  