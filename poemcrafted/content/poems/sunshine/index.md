---
title: "Sunshine"
date: 2024-07-02T12:10:40-06:00
draft: false
---
Amicable embracing _ciel_   
Climbing the hill   
Windblown ligule    
Surrendered to the ground   
Draped by your allure   
Sunbeams dazzling the wake. 

Vibrant _abeilles_ in the uptake    
Visions of a hummingbird    
All quiet, in contemplation 
Of your smile   
Drizzling tears falling 
From the image of the moon. 

Overfilled with joy 
_Tel un soleil_ beaming  
On a bouquet    
_Et_ the hues    
Blasting your eyes  
_Tel un mirage_ from the sky.   

In your presence, nature hushes,    
Ligules surrender to the ground,    
Joy beams _Tel un soleil_   
Serene _et_ lushes  
Peace _et_ calm,    
in every sound. 


