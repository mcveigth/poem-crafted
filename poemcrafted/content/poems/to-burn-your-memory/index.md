---
title: "To Burn Your Memory"
date: 2024-10-19T05:45:10-06:00
draft: false
---
Easily, erase your face,        
make the smiles disappear,      
the places we went together.        
Press, erase,       
and hope that your footprints          
fade from my mind.      

Oh,     
if only it were as easy as selecting every      
photo where your face and mine      
laugh,      
dream,      
kiss,       
share life.     

And with a snap, everything would disappear.        
From my phone at least,         
your memory would die.      

If only I could do the same     
In my heart.        