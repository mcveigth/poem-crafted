---
title: "Ambivalence Is Your Smile"
date: 2024-12-09T16:31:17-07:00
draft: false
---
Just bright, etched with love,   
my indifferent friend.          
Yours, a passionate, cold gaze,          
unaware of my vibrant eyes.         

Sun burning in that frozen spring,      
the fire quenched by the same flame,    
amid lips torn from fragile effort,     
pleading for a kiss.    

Ignorant of reality,    
lost in glances not meant for me,   
and fleeting touches drifting away      
which depart from you, woman     

I firmly hold on to a maybe     
to a lucid smile given in bed       
between scraps of clueless anguish      
and the dew of your silken hands.       

