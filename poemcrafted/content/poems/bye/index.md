---
title: "Bye"
date: 2024-05-18T00:56:00-06:00
draft: false
---

_In you, I found such a deep connection,_  
_something I feel you don’t share with me anymore,_  
_that our souls have been wandering,_  
_shipwrecked in a sea of uncertainty,_  
_I miss how we shared songs,_  
_giving a portion of our souls,_  
_I tell you keep them, don't throw them away,_  
_keep them for me, I'll keep yours,_  
_and even if you leave,_  
_I still have your soul,_  
_you have mine,_  
_take care of it._  