---
title: "Flowers 2"
date: 2024-05-18T01:15:18-06:00
draft: false
---

_I have always hated flowers, not because they are not beautiful,_  
_capable of illuminating ones life_  
_but because they soon fall without consolation_  
_like a mad love that loves so strongly that it extinguishes the embraces of a past love._  
_that's why I prefer them plastic,_  
_even if they look false, and their light does not illuminate your eyes with the same tenderness_  
_at least I know they'll be there, even when I'm gone._  

{{< figure src="flowers-2.png" title="Drawing by Naomi S" height="500" >}}