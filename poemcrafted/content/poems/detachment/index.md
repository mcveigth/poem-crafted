---
title: "Detachment"
date: 2024-10-19T05:39:53-06:00
draft: false
---
Not yearning for the soft fragrance of your hair        
The sweet tones of your smooth skin     
The tender gaze of hazel eyes       

Thinking of anything but your warm voice        
Your gentle smile       
The wavy hair of cold clouds        
Your cheeks, rich in pinks and reds  
Like fragrant autumn        
And     
Pumpkin spice flavour that rests on your lips      

Missing other words but not     
Your words in the empty hours       
In the short times of our fleeting friendship      

Not thinking of your kisses     
Every day       
And how much I’d want to be with you        

Getting lost in messages that aren't yours    
In other touches        
For my own well-being       

And believing that maybe, I'll conquer the world        
But never a glance of love in your eyes     
When they meet mine,        
Which still shine, though you don't see.        