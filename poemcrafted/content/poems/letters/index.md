---
title: "Letters"
date: 2024-05-18T00:13:50-06:00
draft: false
---

_I would like to tell you so much,_   
_but it's useless to write these letters_  
_with no One to hear them,_
_my voice burns as my tears dry up._  

_What a murmur is my tone_  
_It is worth little to tell you, today I saw_  
_your soft face,_  
_your short hair,_  
_Curly smile,_  
_Pale yellowish skin like a December sky_  
_With two emerging moons provocative coffee colour like the one you like to drink so much in the cold mornings_   
_which I won't be there._  

_I would like to tell you something,_  
_but_  
_my shade is dry, bluish, and raspy._  
_The amplitude of my words is attenuated just as it happens to the deep aphonic sound my mouth emits._  
_what's the use of saying it if...if…if you will never feel my voice._   
_And I surrender to the idea that my mute voice will never reach your ears._