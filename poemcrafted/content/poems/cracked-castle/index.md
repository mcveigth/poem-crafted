---
title: Cracked Castle
date: 2024-10-11T00:38:39-06:00
draft: false
---

Let me enter your castle tonight    
Wrap me in your arms    
And drown me in your kisses 

Give me the warmth my soul needs    
Lose yourself in my eyes    
In my deep gaze    
Maybe then you won’t let me go so soon  

I want your claws to anchor into my back    
So I don’t fall from your castle and shatter into pieces    
Let me stay for a long while    
Lull my dreams on your bare chest   

I want to feel your soul tonight    
And think that maybe, just maybe,   
I’ll stay forever.  