---
title: "How Hard Is It to Say I Love You"
date: 2024-05-18T01:02:30-06:00
draft: false 
---

_It is tough to see you_  
_falling,_  
_crying,_  
_In those lonely nights,_  
_where there is no warmth,_  
_In the cold absence of your broken heart,_  
_pushing to survive,_  
_where there's only a pillow, and the noise in your head as company_  
_how hard it is to say I love you,_  
_Looking into your eyes,_  
_with the burden of being aggravating in the heart._  