---
title: "Sphere"
date: 2024-05-18T01:40:18-06:00
draft: false
---

_A jigsaw puzzle of small pieces that pass between my hands._  
_From pieces,_   
_I build its distant silhouette,_   
_who would have thought that under pieces of_     
_cloth I would find such a delicate outline_  
_and that among_  
_a thousand bits I would have to assemble it that carved thick edge,_  
_that among fragments I would join with my delicate hands such a fine_  
_soft and whitened outline that with a look evokes such beautiful feelings,_  
_stirs the deepest of my innermost being._  

{{< figure src="sphere.png" title="Drawing by Naomi S" height="500" >}}