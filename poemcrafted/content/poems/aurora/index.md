---
title: "Aurora"
date: 2024-05-18T01:29:30-06:00
draft: false
---

_Tonight,_  
_the dawn dazzled my being,_  
_As the firmament embraced the evening lights._  
_In my mind, you appeared,_  
_I wondered what you were doing at that moment._  
_You, who longed to contemplate the night like this,_  
_Radiant, full of shades and hues._  
_Red, like your burning passion,_  
_Green, like your constant obstinacy,_  
_Violet, like your unique essence._  

_The colors of the sky shimmer,_  
_And in their radiance, my heart plunged in nostalgia._  
_Though my heart loves you differently now_  
_Your memory still dances in my thoughts._  
_I wonder what you are doing at this moment,_  
_You, who once embraced the sky with light and colour._  
_Perhaps you are now shining down unknown paths,_  
_Or maybe you seek refuge on another hemisphere._  
_Though the echo of your laughter fades in the mist of the night,_  
_A part of me still clings to your light,_  
_For despite fading from the blinding night_  
_I can't help but wonder what you're after now._  
