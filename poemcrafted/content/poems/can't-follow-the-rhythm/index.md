---
title: "Can't Follow the Rhythm"
date: 2024-05-18T00:59:33-06:00
draft: false
---

_A strange world_  
_Full of swing and jazz_  
_Of pain and glass_  
_On the bus of whose dream_  
_I can never forget_  
_Tell me you're from here, shout that you get me._  
_That in the singing of a child all is over_  
_to whom escape from the torment, from the burden of living_  
_my griefs and lamentations,_  
_please be gone,_  
_that in the cries of my mother_  
_the night lulls me to sleep_  
_let me breathe!_  
_Not to hear her cries_  
_neither mine_  
_play the music loud, but burn my timpani_  
_I don't want to listen to you no longer,_  
_nor feel my heartbeat,_  
_only the rhythm of a song:_  
_twelve one two ... I forgot!_  
-three four five... where am I!_  