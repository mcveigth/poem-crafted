---
title: "Those Days"
date: 2024-10-19T05:42:52-06:00
draft: false
---
There are days when I don't even understand myself,     
When I’m consumed by a feeling of   

Fear.   

An immeasurable terror of what’s imminent,  
Those days when even the blowing wind   
Can make me cry.    

Days of little calm,    
Of anguish and sorrow,  
In which I wish to lose myself in my dreams,    
In the warmth of my sheets and never leave again.   

Those nights when no one hears me,          
No one sees me,     
No one thinks of me,        
Deep in the cave of my own mess,        
I drown.        

Yes,    
In a glass of water.    

Those days when I’m just a nuisance     
And all I want is to fight the world,       
When I become passive-aggressive        
And throw barbs without thinking.       

On those days, that’s when I really need        
A little affection,     
Some understanding,     
A strong and long hug,      
And a lie:      
“Everything will be okay.”      

And I know it won’t be,     
But my soul is calmed by feeling your arms,     
The warmth of care,     
Your care.      