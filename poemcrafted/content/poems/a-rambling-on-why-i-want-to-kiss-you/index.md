---
title: "A Rambling on Why I Want to Kiss You"
date: 2024-12-18T16:41:07-07:00
draft: false
---
Your lips meet mine with a tender fire,     
A rush of passion, a fierce desire.     
Our hearts align in a hurried beat,     
Your hands on me, the world retreats.       

Through dreamlike realms, I’m swept away,   
Your hazel eyes, where yearnings play.  
The scent of your curls, a fragrant art,    
A spell that binds and claims my heart. 

Upon your chest, I find my rest,    
The warmth, the solace, I love the best.    
The essence rising from your skin,  
A cocktail sweet that draws me in.  