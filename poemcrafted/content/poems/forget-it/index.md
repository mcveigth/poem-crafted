---
title: "Forget It"
date: 2024-05-18T01:06:47-06:00
draft: false
---

_I Breathe_  
_Should I care?_  
_They're all gone_  
_Should I Care?_  
_Lull me to sleep You're my only comfort_  
_Should I care?_  
_Two breaths_  
_Should I care?_  
_Don't leave me alone_  
_Should I care?_  
_Hard comfort that you offer_  
_Should I care?_  
_Third breath, I despair_  
_Should I care?_  
_Warm night turned cold_  
_And should I care?_  
_Give me one dream, one hope_  
_One soul, one life._  
_**SOMETHING**_  
_Should I care?_  
_Give me a rope_  
_Whether to hang me or save me_  
_A snowflake_  
_Should I care?_  
_Throw me to the ground,_  
_Look me in the eyes,_  
_Say my name and love me_  
_Should you care!_  
_Fourth breath, I remembered I don't have you_  
_Should, I care?_  
_Bite me burn me_  
_To make it matter_  
_Start hating me,_  
_Should I care?_  
_Spit on me, cut me_  
_As always_  
_Kill me or cure me_  
_Should you care!_  
_Lead me to Heaven_  
_Condemn me to hell_  
_I breathe,_  
_It doesn't matter anymore,_  
_Forget me, forget it._  

{{< figure src="forget-it.png" title="Drawing by Naomi S" height="500" >}}