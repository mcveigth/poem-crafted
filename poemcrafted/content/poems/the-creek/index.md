---
title: "The Creek"
date: 2024-10-11T00:28:31-06:00
draft: false
---

Lost in the forest, we are  
Amid mist and branches  
Walking aimlessly

Lost, we are  
We have no idea  
Where to go  
We don’t want to leave the forest  
A thousand and one paths  
Which should we choose?

We enter one  
We leap to another  
And you,  
Your path is the creek  
Among the mud  
You go

You go without me

In the dark mist  
I can only hear  
The trees calling  
Shouting  
Singing a song without rhythm  
That only the stars can understand

They call me to run, to flee  
To avoid your path  
The muddy road you’ve created

But, I look at you  
I look into your eyes  
And the only thing I can understand  
Is the faint glimmer in them  
Calling me  
Asking me to stay  
To get lost  
In a path with no direction

You start to hum  
And your voice blends with the trees  
Murmuring the same  
Singing to the stars

And I,  
I want to leap  
To enter the muddy path  
I no longer care about anything

I don’t care about getting lost  
So I throw myself into it  
I start to walk  
And I don’t see you there

You are gone  
You took another path  
I only see the shadows that are no longer us.
