---
title: "The Scars of The River"
draft: false
date: 2024-12-27T17:05:02-07:00
---
The river flowed, cloaked in shadows, through the mountains surrounding the heart of the city. A girl watched a scrawny, unassuming cat approach her. She had always been a lover of animals, having three dogs, two rabbits, and now this thin black cat crossing the river under the shadows of white cedars and guava trees.

This was one of the many mountains surrounding a relatively suburban town called Rionegro, in Antioquia, Colombia. A place with a warm climate, shaded by towering oaks, cedars, and guava trees, its colonial architecture reminiscent of the remnants of old Spain.

The girl lived in one of those ancestral houses that once belonged to her family. She loved how the forest seemed to overtake the town, except for the river, darkened by shadows, that connected and divided the city.

I met her in Bogotá, at a low-class café where I worked as a waiter. She was a woman with a soft face, dark hair cascading in waves, and piercing onyx eyes. We talked for a while, and, in one of those rare moments of connection, she shared her past with me, as if displaying the scars of her victories with the pride of a warrior. Many details drifted away in the air of the conversation, my mind distracted by her pale pink lips. But one story resonated deeply within me: how her first love stripped her of dignity, leading her into loneliness—or perhaps it was the other way around?

The flames burned in her coal-black eyes as her memories turned to ashes on the incandescent photo paper and letters of the one she once loved. She had met him at school, and after brief glances of mutual desire, they started dating. Like any young, naive love, they promised eternal devotion. The first few months were like sugarcane—sweet smiles and lost in the mist of the forest. She loved him with cloying sweetness, but he saw her as a naive girl to amuse himself with.

"There were so many things on my mind when, suddenly, he changed, as if he had subtly started hunting me. A quiet game of manipulation and greed, kind one day, withered like a rose the next. So much insistence... I couldn’t take it anymore. I loved him and didn’t want to lose him. You know what I mean?"
On a remote lookout where the horizon embraced the mountains, he took her. And amidst cheap poetry, he tore her clothes and consumed her body. A few days later, with flimsy excuses, he ended things. Her tears overflowed the moon, flowing into the river, and she vaguely sought solace in some herb, helping her lose herself, escape herself. She became addicted—not just to him, but to the faint relief of a few pesos and a trip to the slums, where she could find any concoction to eclipse the moon. At times, he sought her out to sate his hunger, and she, in desperate loneliness, would let him, even when he had begun consuming others.

She distanced herself from everyone as the pain in her heart suffocated her breath. She wandered the dark streets of the city, doing things "just for fun, as a way to ignore my pain so no one could see me." One day, near the river, she sat and began to play with the water, moving her fingers slowly, feeling the coldness pierce her skin. She picked up a stone and threw it into the water, watching her reflection distort in the ripples. She lay back and cried. By chance, she might have seen a battered cat crossing the river, dripping with pain, the water tinged crimson from its wounded paw. She took it in her arms and saw the open wound on its leg. In an act of tenderness, she brought it home, disinfected its wounds, and gave it some food. “I called him Bruno. He was so much like me, you know? But what made us different was that he kept trying to cross the river, to keep going. Even when there was no reason to fight, and his wound was so bad it might have cost him his life, he kept moving forward.”

She stood up and wiped the thick, salty moonlight from her cheeks. “I grabbed a bucket, looked at Bruno, smiled, and grabbed some matches.” In the backyard, she lit a fire. The smoke rose as she threw in the rotting remnants that had weighed on her soul. “It wasn’t easy. Don’t think that burning some photos and letters fixed everything immediately. You always question your soul, your heart, your worth. There’s a dark scar, deep, within our fractured hearts. But something has to be done; you have to keep moving forward and try.”

Her life afterward wasn’t easy. It took a lot of time and many attempts to leave the past behind, and the lessons from her first bad love weren’t enough for the second or third. Yet she bore her dark scars with pride, and when I met her in that café, she was someone better than yesterday, but perhaps worse than tomorrow.


