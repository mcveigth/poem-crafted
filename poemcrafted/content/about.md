---
title: "About Me"

---

I believe it's appropriate to introduce myself and provide some background information. Starting with the basic and somewhat cliché details typically expected in an introduction: My name is Mcveigth; I'm 20 years old and spent most of my life in Colombia before moving to Canada. I'm fluent in two languages, English and Spanish, and currently, I'm attempting to learn Japanese. I'm majoring in mathematics and in general I have a great interest in computers and books and that sort of stuff.

Writing and making poems is a bit of a hobby of mine and also my way of coping with the adversity of life. And that's pretty much all I'm going to share for now on this 'about me' page.