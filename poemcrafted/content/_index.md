---
title: "Welcome to PoemCrafted"

---

# Welcome to PoemCrafted
Hiya! This is PoemCrafted, a little website where I index poems I've been making over the years. Notice how I said 'making' rather than 'writing' because for me, there's a subtle difference between the two: making a poem involves crafting it with care, attention, and emotion, while writing a poem feels more like simply putting words on paper(or rather bytes).
## Why posting here?
Well... why not? I often share my poems with friends or people I meet along the way and I think this is a better way to share those poems with the world. Of course, they aren't masterpieces hell some of them are crap. However, if even one of them can touch someone move something in the heart, mind or soul of a person I'll be more than content with it. I tend to write the most when I'm heartbroken, anxious or pondering about life at 3am while looking at the obliqueness of the ceiling, thus bear in mind some of them are a bit edgy and depressing.

For me, they are a way to communicate the things I can't easily share with others perhaps because at times I struggle to convey my ideas or rather I feel they aren't completely understood. But that's the magic about poems--they are complicated, tangled, seemingly nonsensical at first. Some of them full of meanings, not one or two but multiple ways to read and interpret them. And **YET** they can express **EVERYTHING**.

With not much to say for now, enjoy.