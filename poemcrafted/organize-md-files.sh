#!/bin/bash

# Print current working directory for debugging
echo "Current working directory: $(pwd)"

# List all .md files in the current directory
for file in *.md; do
    # Check if there are no .md files
    if [[ "$file" == "*.md" ]]; then
        echo "No .md files found in the current directory."
        exit 1
    fi

    # Remove the .md extension to get the folder name
    folder_name="${file%.md}"

    # Debug: print the folder name
    echo "Creating folder: $folder_name"

    # Create the folder if it doesn't exist
    mkdir -p "$folder_name"

    # Move the .md file into the corresponding folder
    mv "$file" "$folder_name/"

    # Debug: confirm the move
    echo "Moved $file to $folder_name/"
done
