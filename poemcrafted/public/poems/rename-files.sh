#!/bin/bash

# Loop through each directory in the current directory
for directory in */; do
    # Check if the directory is a directory (excluding symbolic links)
    if [ -d "$directory" ] && [ ! -L "$directory" ]; then
        # Check if the directory contains a file named "folderX.md"
        if [ -f "$directory/${directory%/}.md" ]; then
            # Rename "folderX.md" to "index.md"
            mv "$directory/${directory%/}.md" "$directory/index.md"
            echo "Renamed $directory/${directory%/}.md to $directory/index.md"
        fi
    fi
done
