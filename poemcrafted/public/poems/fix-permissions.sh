#!/bin/bash

# Get the current user and group
USER=$(whoami)
GROUP=$(id -gn)

# Ensure the script is run with the correct permissions
if [ "$(id -u)" -eq 0 ]; then
    echo "Please run this script as a regular user, not as root."
    exit 1
fi

# Check if a directory argument is provided
if [ -z "$1" ]; then
    echo "Usage: $0 /path/to/directory"
    exit 1
fi

# Change ownership of all directories and files in the specified directory
sudo chown -R "$USER":"$GROUP" "$1"

# Confirm the change
echo "Changed ownership of all files and directories in $1 to $USER:$GROUP"
